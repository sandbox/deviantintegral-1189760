NARP for Drush
==============

This Drush command provides an alternate method of rebuilding node access
tables for sites with many nodes.

Advantages over core's node access rebuild mechanisim include:

 * Not deleting all access grants from {node_access} when rebuilding grants.
   This means that an action that will change grants will not cause the
   previous access state to be wiped at once, preserving user experience during
   the rebuild.
 * Doesn't hardcode the time limit of the rebuild, so it can run for however
   long Drush is allowed to run based off of your php.ini for the PHP CLI.
 * Can be run in parallel with cron so that multiple CPU cores are used, or so
   that it can be run in parallel across multiple machines.

Installation
============

 0. Currently, NARP has only been tested with Drupal 6. Drupal 7 patches
    welcome!
 1. Download narp.drush.inc to ~/.drush
 2. Add the following line to cron for each job you want to run in parallel. I
    suggest running one less job than CPU cores on the server.

    * * * * * drush -q narp

 3. Tweak the command as needed based on the performance of the server.
    Consider running the command once every 5 minutes, or changing the default
    limit of nodes to process at once with the --limit parameter.
 4. To force rebuilding of the node access table, use the --reset parameter.

Status
======

The easiest way to check on the status of the node access table is to run:

 $ drush narp --limit=1

Or, to see the list of nodes queued to be rebuilt:

 $ drush cache-get narp_queue

