<?php

/**
 * Implements hook_drush_command().
 */
function narp_drush_command() {
  $items = array();
  $items['node-access-rebuild-parallel'] = array(
    'description' => dt('Rebuild the node access tables while supporting parallel execution.'),
    'aliases' => array('narp'),
    'options' => array(
      'reset' => dt('Reset the queue of nodes to rebuild.'),
      'limit' => dt('Limit to rebuilding this number of nodes.'),
    ),
  );

  return $items;
}

/**
 * Rebuild the node access table.
 *
 * This function pulls a set of nodes from the queued set of nodes to rebuild
 * and rebuilds them. Multiple instances of drush can be running this command
 * at once.
 */
function drush_narp_node_access_rebuild_parallel() {
  // Either the user has explicitly asked to rebuild node access, or some other
  // process has triggered a rebuild.
  if (drush_get_option('reset') || node_access_needs_rebuild()) {
    _narp_set_queue();
  }
  $limit = drush_get_option('limit') ? (int)drush_get_option('limit') : 1000;
  $nodes = _narp_get_queue($limit);
  if (!empty($nodes)) {
    drush_log(dt("Rebuilding access for nodes !low to !high.", array('!low' => min($nodes), '!high' => max($nodes))), 'ok');
    _narp_execute($nodes);
  }
  else {
    drush_log(dt("There are no nodes queued to be rebuilt."), 'ok');
  }
}

/**
 * Set the queue of nodes needing to have their node access rebuilt. Note that
 * this function and _narp_get_queue() both require a mutex to prevent multiple
 * processes from modifying the queue.
 */
function _narp_set_queue() {
  while (!lock_acquire('narp_queue')) {
    // If we couldn't get a lock, sleep between one and 5 seconds.
    sleep(rand(1, 5));
  }

  // Mark the node access table as not needing to be rebuilt, since we are
  // doing it ourselves.
  node_access_needs_rebuild(FALSE);
  $result = db_query("SELECT nid FROM {node} ORDER BY nid");
  $nodes = array();
  if (drush_drupal_major_version() >= 7) {
    $nodes = $result->fetchCol();
  }
  else {
    while ($nid = db_result($result)) {
      $nodes[] = (int)$nid;
    }
  }
  // We can't use the variable system due to the static variable cache.
  cache_set('narp_queue', $nodes);
  lock_release('narp_queue');
}

/**
 * Fetch a set of nodes to rebuild the node access table for.
 *
 * @param $limit
 *   Fetch at most $limit nodes.
 *
 * @return
 *   An array of node IDs to rebuild the access table for.
 */
function _narp_get_queue($limit) {
  while (!lock_acquire('narp_queue')) {
    // If we couldn't get a lock, sleep between one and 5 seconds.
    sleep(rand(1, 5));
  }

  if ($cached = cache_get('narp_queue')) {
    $nodes = $cached->data;
    if (is_array($nodes) && !empty($nodes)) {
      $subset = array_splice($nodes, 0, $limit);
      cache_set('narp_queue', $nodes);
    }
  }
  lock_release('narp_queue');
  return $subset;
}

/**
 * Rebuild the node access grants for a set of nodes.
 *
 * @param $nodes
 *   An array of node IDs to rebuild.
 */
function _narp_execute($nodes) {
  // Try to allocate enough time to rebuild node grants.
  if (function_exists('set_time_limit')) {
    @set_time_limit(0);
  }

  $good = array();
  $bad = array();

  // Only recalculate if the site is using a node_access module.
  if (count(module_implements('node_grants'))) {
    // Delete the default grant, if it exists.
    if (drush_drupal_major_version() >= 7) {
      db_delete('node_access')
        ->condition('nid', 0)
        ->execute();
    }
    else {
      db_query("DELETE FROM {node_access} WHERE nid = 0");
    }

    timer_start('narp_rebuild');
    $last_interval = 0;
    foreach ($nodes as $nid) {
      $loaded_node = node_load($nid, NULL, TRUE);
      // To preserve database integrity, only aquire grants if the node
      // loads successfully.
      if (!empty($loaded_node)) {
        node_access_acquire_grants($loaded_node);
        $good[$nid] = TRUE;
        if (drush_get_option('verbose')) {
          drush_log(dt('Rebuilt permissions for node !nid.', array('!nid' => $nid)), 'ok');
        }
        elseif (!drush_get_option('quiet') && (count($good) % 50 == 0)) {
          print('*');
        }
      }
      else {
        $bad[$nid] = TRUE;
        if (!drush_get_option('quiet')) {
          print('!');
        }
      }

      if (count($good) >= 500 && count($good) % 100 == 0) {
        // Convert time from ms to seconds.
        $running_time = timer_read('narp_rebuild') / 1000;
        $interval = round($running_time, -1);
        if (($interval % 10 == 0) && ($interval > $last_interval)) {
          $last_interval = $interval;
          $eta = $running_time * (count($nodes) - count($good)) / count($good);

          if (!drush_get_option('quiet')) {
            print("\n");
          }

          drush_log(dt('@good of @count nodes have had their permissions rebuilt in @time. ETA: @eta',
            array(
              '@good' => count($good),
              '@count' => count($nodes),
              '@eta' => format_interval(round($eta)),
              '@time' => format_interval(round($running_time, -1)),
            )
          ), 'ok');
        }
      }
    }
  }
  else {
    // Not using any node_access modules. Add the default grant.
    drush_log(dt('There are no node_access modules. Adding the default grant.'), 'ok');
    if (drush_drupal_major_version() >= 7) {
      db_delete('node_access')->execute();
      db_insert('node_access')
        ->fields(array(
          'nid' => 0,
          'realm' => 'all',
          'gid' => 0,
          'grant_view' => 1,
          'grant_update' => 0,
          'grant_delete' => 0,
        ))
        ->execute();
    }
    else {
      db_query("DELETE FROM {node_access}");
      db_query("INSERT INTO {node_access} VALUES (0, 0, 'all', 1, 0, 0)");
    }
  }


  if (!drush_get_option('quiet')) {
    print("\n");
  }

  drush_log(dt('Rebuilt permissions for @count nodes. @good nodes were correctly rebuilt and @bad nodes failed to properly load.',
    array(
      '@count' => count($nodes),
      '@good' => count($good),
      '@bad' => count($bad),
    )
  ), 'ok');

  if (count($bad) > 0) {
    return drush_set_error('DRUSH_ERROR_CODE', dt("Here is the list of node IDs that failed to load: \n@badlist", array('@badlist' => implode("\n", array_keys($bad)))));
  }

}

